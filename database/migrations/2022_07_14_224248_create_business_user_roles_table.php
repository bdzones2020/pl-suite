<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('business_user_roles', function (Blueprint $table) {
            $table->id();
            $table->integer('business_id')->nullable()->index()->unsigned();
            $table->string('name')->nullable();
            $table->string('slug')->nullable();
            $table->double('weight', 4,2)->nullable()->default(9.99);
            $table->text('short_details')->nullable();
            $table->text('permissions')->nullable();
            $table->boolean('status')->nullable()->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('business_user_roles');
    }
};
