<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('businesses', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id')->index()->nullable()->unsigned();
            $table->string('business_name')->nullable();
            $table->string('business_slug')->nullable();
            $table->string('business_logo')->nullable();
            $table->string('business_type')->nullable();
            $table->string('business_code')->nullable()->unique();
            $table->string('business_contact_numbers')->nullable();
            $table->string('business_email')->nullable()->unique();
            $table->string('business_address')->nullable();
            $table->double('business_rating',5,2)->nullable()->default(0.0);
            $table->string('business_status')->nullable()->default('active');
            $table->string('subscribed_package')->nullable()->default('TRIAL');
            $table->integer('package_weight')->nullable()->default(0);
            $table->boolean('package_status')->nullable()->default(false);
            $table->integer('max_users')->nullable()->default(1);
            $table->date('valid_till')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('businesses');
    }
};
