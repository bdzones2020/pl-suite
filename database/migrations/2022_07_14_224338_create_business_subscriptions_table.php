<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('business_subscriptions', function (Blueprint $table) {
            $table->id();
            $table->integer('business_id')->nullable()->index()->unsigned();
            $table->string('package')->nullable();
            $table->string('price_policy')->nullable();
            $table->double('amount',15,2)->nullable()->default(0.0);
            $table->string('transaction_method')->nullable();
            $table->string('transaction_code')->nullable();
            $table->string('transaction_mobile_number')->nullable();
            $table->double('paid_amount',15,2)->nullable()->default(0.0);
            $table->string('status')->nullable();
            $table->date('valid_till')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('business_subscriptions');
    }
};
