<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('business_settings', function (Blueprint $table) {
            $table->id();
            $table->integer('business_id')->nullable()->index()->unsigned();
            $table->string('base_currency')->nullable()->default('BDT');
            $table->string('base_language')->nullable()->default('EN');
            $table->boolean('stock_dependency')->nullable()->default(false);
            $table->boolean('web_page')->nullable()->default(false);
            $table->boolean('email_notification')->nullable()->default(false);
            $table->boolean('sms_notification')->nullable()->default(false);
            $table->boolean('has_employee_management')->nullable()->default(false);
            $table->boolean('enable_stock_limit_warning')->nullable()->default(false);
            $table->boolean('allow_online_order')->nullable()->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('business_settings');
    }
};
