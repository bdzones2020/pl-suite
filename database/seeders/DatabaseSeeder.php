<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Business\BusinessUserRole;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);

        $passSystem = 789456123;
        $userSystem = User::create([
            'name' => 'System Admin',
            'email' => 'system.admin@mailinator.com',
            'type' => 'system',
            'weight' => 99.99,
            'password' => bcrypt($passSystem),
        ]);

        $business_user_role = BusinessUserRole::create([
            'name' => 'Owner',
            'slug' => 'owner',
            'weight' => 99.99,
            'short_details' => 'The owner is the super admin and unchangable to this business solutions.',
            'permissions' => 'dashboard,all',
            'status' => true,
        ]);
    }
}
