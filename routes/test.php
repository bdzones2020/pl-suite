<?php

use App\Http\Controllers\TestController;

Route::get('/test/old-db/{businessSlug}', [TestController::class, 'old_db']);
Route::get('/test/check-mail', [TestController::class, 'check_mail']);
Route::get('/test/check-sms', [TestController::class, 'check_sms']);