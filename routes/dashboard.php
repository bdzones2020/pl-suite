<?php

use App\Http\Controllers\Dashboard\DashboardController;

Route::group(['prefix' => '/{businessSlug}/', 'as' => 'dashboard.' , 'middleware' => ['auth','dashboard']], function () {

	Route::get('/', [DashboardController::class, 'index'])->name('/');
	Route::get('/help-guide', [DashboardController::class, 'help_guide'])->name('help-guide');
	// business info
	Route::group(['prefix' => '/business-info'],function(){
		Route::get('/', [BusinessController::class, 'business_info'])->name('business-info');
		Route::get('/edit', [BusinessController::class, 'business_info_edit'])->name('business-info.edit');
		Route::post('/update', [BusinessController::class, 'business_info_update'])->name('business-info.update');
		// business settings
		Route::get('/settings/edit', [BusinessController::class, 'business_settings_edit'])->name('business-info.settings.edit');
		Route::post('/settings/update', [BusinessController::class, 'business_settings_update'])->name('business-info.settings.update');
		// business subscription
		Route::get('/subscription/edit', [BusinessController::class, 'business_subscription_edit'])->name('business-info.subscription.edit');
		Route::post('/subscription/update', [BusinessController::class, 'business_subscription_update'])->name('business-info.subscription.update');
	});

	// business-user-roles routes
	Route::resource('/business-user-roles', BusinessUserRoleController::class);

	Route::resource('user', UserController::class);

});