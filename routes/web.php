<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\WebController;

Route::get('/clear-parasites', function () {
    $exitCode = Artisan::call('optimize:clear');
    return back();
});

Route::get('/', [WebController::class, 'home'])->name('/');
Route::get('/home', [WebController::class, 'home'])->name('home');
Route::get('/about-us', [WebController::class, 'about_us'])->name('about-us');
Route::get('/services', [WebController::class, 'services'])->name('services');
Route::get('/pricing', [WebController::class, 'pricing'])->name('pricing');
Route::get('/faq', [WebController::class, 'faq'])->name('faq');
Route::get('/terms-&-conditions', [WebController::class, 'terms_conditions'])->name('terms-&-conditions');
Route::get('/privacy-policy', [WebController::class, 'privacy_policy'])->name('privacy-policy');
Route::get('/contact', [WebController::class, 'contact'])->name('contact');
Route::post('/send-message', [WebController::class, 'send_message'])->name('send-message');

require __DIR__ . '/test.php';

Auth::routes();

require __DIR__ . '/system.php';
require __DIR__ . '/dashboard.php';