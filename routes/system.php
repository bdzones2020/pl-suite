<?php

use App\Http\Controllers\System\SystemController;
use App\Http\Controllers\System\BusinessController;
use App\Http\Controllers\System\SubscriptionController;
use App\Http\Controllers\System\UserController;

Route::group(['prefix' => '/system', 'as' => 'system.' , 'middleware' => ['auth','system']], function () {

	Route::get('/', [SystemController::class, 'index'])->name('/');
	// buisness
	Route::get('/business/settings/{id}', [BusinessController::class, 'settings'])->name('business.settings');
	Route::resource('/businesses', BusinessController::class);
	// business subscriptions
	Route::resource('/subscriptions', SubscriptionController::class);
	Route::resource('/users', UserController::class);

});