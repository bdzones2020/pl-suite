@extends('web.index')

@section('title', 'Pricing')

@section('web_content')

    <section id="pricing" class="pricing">
        <div class="container">
            <div class="section-title">
                <h2>Pricing</h2>
                <h5 class="text-dark">Please <a href="{{ route('register') }}" class="text-primary" title="register"><strong>register</strong></a> now to get <strong class="text-primary">60 days free trial</strong>.</h5>
            </div>

            <div class="row">

                <div class="col-lg-4 col-md-6">
                    <div class="box recommended">
                        <h3>STARTER</h3>
                        <hr class="mt-0 mb-2">
                        <ul>
                            <li>MEASUREMENTS</li>
                            <li>ADDONS</li>
                            <li>DESIGNS</li>
                            <li>ORDERS</li>
                            <li>FABRICS</li>
                            <li class="na">STOCKS</li>
                            <li class="na">BRANDS</li>
                            <li class="na">USERS</li>
                            <li class="na">VENDORS</li>
                            <li class="na">CUSTOMERS</li>
                            <li class="na">WEB PAGE</li>
                            <li class="na">EMAIL NOTIFICATION</li>
                            <li class="na">SMS NOTIFICATION</li>
                        </ul>
                        <h4 class="row text-center">
                            <div class="col">
                                400 {{ config('app.base_currency') }}<span> / month</span>
                            </div>
                            <div class="col">
                                4000 {{ config('app.base_currency') }}<span> / year</span>
                            </div>
                        </h4>
                        <div class="btn-wrap">
                            <a href="#" class="btn-buy">Subscribe Now</a>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6">
                    <div class="box recommended">
                        <h3>STANDARD</h3>
                        <hr class="mt-0 mb-2">
                        <ul>
                            <li>MEASUREMENTS</li>
                            <li>ADDONS</li>
                            <li>DESIGNS</li>
                            <li>ORDERS</li>
                            <li>FABRICS</li>
                            <li>STOCKS</li>
                            <li>BRANDS</li>
                            <li>USERS</li>
                            <li class="na">VENDORS</li>
                            <li class="na">CUSTOMERS</li>
                            <li class="na">WEB PAGE</li>
                            <li class="na">EMAIL NOTIFICATION</li>
                            <li class="na">SMS NOTIFICATION</li>
                        </ul>
                        <h4 class="row text-center">
                            <div class="col">
                                800 {{ config('app.base_currency') }}<span> / month</span>
                            </div>
                            <div class="col">
                                8000 {{ config('app.base_currency') }}<span> / year</span>
                            </div>
                        </h4>
                        <div class="btn-wrap">
                            <a href="#" class="btn-buy">Subscribe Now</a>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6">
                    <div class="box recommended">
                        <h3>PREMIUM</h3>
                        <hr class="mt-0 mb-2">
                        <ul>
                            <li>MEASUREMENTS</li>
                            <li>ADDONS</li>
                            <li>DESIGNS</li>
                            <li>ORDERS</li>
                            <li>FABRICS</li>
                            <li>STOCKS</li>
                            <li>BRANDS</li>
                            <li>USERS</li>
                            <li>VENDORS</li>
                            <li>CUSTOMERS</li>
                            <li>WEB PAGE</li>
                            <li>EMAIL NOTIFICATION</li>
                            <li>SMS NOTIFICATION</li>
                        </ul>
                        <h4 class="row text-center">
                            <div class="col">
                                1500 {{ config('app.base_currency') }}<span> / month</span>
                            </div>
                            <div class="col">
                                15000 {{ config('app.base_currency') }}<span> / year</span>
                            </div>
                        </h4>
                        <div class="btn-wrap">
                            <a href="#" class="btn-buy">Subscribe Now</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>

@endsection