@extends('web.index')

@section('title', 'Terms & Conditions')

@section('web_content')

    <section id="faq" class="faq">
        <div class="container">
            <div class="section-title">
                <h2>Terms & Conditions</h2>
            </div>

            <div class="row faq-item d-flex align-items-stretch fw-bold">
                <div class="col-12">
                    <p class="fs-16px">
                        1 # Tailors Manager has been developed to manage a particular business of tailoring only.
                    </p>
                </div>
            </div>
            
            <div class="row faq-item d-flex align-items-stretch fw-bold">
                <div class="col-12">
                    <p class="fs-16px">
                        2 # We do not provide any online payment gateway, rather the payment is completely manual and designed as per tailoring business required.
                    </p>
                </div>
            </div>

            <div class="row faq-item d-flex align-items-stretch fw-bold">
                <div class="col-12">
                    <p class="fs-16px">
                        3 # User registration is similar to business registration. Your registration contact number or email and your given password are account credential for your dashboard.
                    </p>
                </div>
            </div>

            <div class="row faq-item d-flex align-items-stretch fw-bold">
                <div class="col-12">
                    <p class="fs-16px">
                        4 # This software is not only for commercial purposes, but also for individuals who serve from home or privately, can also register here to get the complete overview of their products and services.
                    </p>
                </div>
            </div>

            <div class="row faq-item d-flex align-items-stretch fw-bold">
                <div class="col-12">
                    <p class="fs-16px">
                        5 # Web page is a default part of this software. This is to display your designs or fabrics or sample products. You can customise your web page on request (payment required).
                    </p>
                </div>
            </div>

            <div class="row faq-item d-flex align-items-stretch fw-bold">
                <div class="col-12">
                    <p class="fs-16px">
                        6 # Default allocated storage by packages are: <br> <br>
                    </p>
                    <p class="fs-16px" style="margin-left: 50px;">
                        STARTER : 200 MB <br>
                        STANDARD: 400 MB <br>
                        PREMIUM: 800MB
                    </p>
                </div>
            </div>

            <div class="row faq-item d-flex align-items-stretch fw-bold">
                <div class="col-12">
                    <p class="fs-16px">
                        7 # You can increase your storage through extra payment (per 200 MB 100 BDT per month).
                    </p>
                </div>
            </div>

            <div class="row faq-item d-flex align-items-stretch fw-bold">
                <div class="col-12">
                    <p class="fs-16px">
                        8 # We will not keep backup of your data if you do not subscribe to any package after the trial period.
                    </p>
                </div>
            </div>

            <div class="row faq-item d-flex align-items-stretch fw-bold">
                <div class="col-12">
                    <p class="fs-16px">
                        9 # Based on your used storage and server cost, pricing may change.
                    </p>
                </div>
            </div>

            <div class="row faq-item d-flex align-items-stretch fw-bold">
                <div class="col-12">
                    <p class="fs-16px">
                        10 # Natural disaster, accident, national or international law issue, copyright policy of your images, wrong operating and like this type of incidents may affect your data and pricing, and obviously we will never take any responsibilities of this type of issue.
                    </p>
                </div>
            </div>

            <div class="row faq-item d-flex align-items-stretch fw-bold">
                <div class="col-12">
                    <p class="fs-16px">
                        11 # Email notification will not work if you do not provide any valid email.
                    </p>
                </div>
            </div>

            <div class="row faq-item d-flex align-items-stretch fw-bold">
                <div class="col-12">
                    <p class="fs-16px">
                        12 # SMS notification is a paid service.
                    </p>
                </div>
            </div>

        </div>
    </section>

@endsection