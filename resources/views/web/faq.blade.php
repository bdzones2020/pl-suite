@extends('web.index')

@section('title', 'FAQ')

@section('web_content')

    <section id="faq" class="faq">
        <div class="container">
            <div class="section-title">
                <h2>Frequently Asked Questions</h2>
            </div>

            <div class="row faq-item d-flex align-items-stretch">
                <div class="col-12">
                    <i class="ri-question-line"></i>
                    <h4>What is Tailors Manager?</h4>
                </div>
                <div class="col-12">
                    <p>
                        Tailors Manager is an online software to manage tailoring business digitally which is available anywhere if there is an internet connection.
                    </p>
                </div>
            </div>

            <div class="row faq-item d-flex align-items-stretch">
                <div class="col-12">
                    <i class="ri-question-line"></i>
                    <h4>Why will the client take service from the Tailors Manager?</h4>
                </div>
                <div class="col-12">
                    <p>
                        This software will remove your manually writing hassles and errors, provide payment management system and tracking of customer orders, payment histories, invoice printing facilities and other boring human effort will also decrease. It also manages customer communication to get the best out of your business.
                    </p>
                </div>
            </div>

            <div class="row faq-item d-flex align-items-stretch">
                <div class="col-12">
                    <i class="ri-question-line"></i>
                    <h4>What if I am an individual tailor, and serve privately without any business centre?</h4>
                </div>
                <div class="col-12">
                    <p>
                        Tailor Manager is built to serve any type of tailoring business even if you have no business centre or internal fabric stock. You will just need to add a business name which may be your own personal name.
                    </p>
                </div>
            </div>

        </div>
    </section>

@endsection