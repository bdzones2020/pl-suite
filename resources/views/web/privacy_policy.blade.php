@extends('web.index')

@section('title', 'Privacy Policy')

@section('web_content')

    <section id="faq" class="faq">
        <div class="container">
            <div class="section-title">
                <h2>Privacy Policy</h2>
            </div>

            <div class="row faq-item d-flex align-items-stretch">
                <div class="col-12">
                    <i class="ri-question-line"></i>
                    <h4>User Data</h4>
                </div>
                <div class="col-12">
                    <p>
                        All the user data are restricted and private to this software.
                    </p>
                </div>
            </div>

            <div class="row faq-item d-flex align-items-stretch">
                <div class="col-12">
                    <i class="ri-question-line"></i>
                    <h4>Business Data</h4>
                </div>
                <div class="col-12">
                    <p>
                        Business data is particularly dedicated to the registered business owner only. So all of your data are secured in your own business panel.
                    </p>
                </div>
            </div>

        </div>
    </section>

@endsection