@extends('web.index')

@section('title', 'About Us')

@section('web_content')

    <section id="about" class="about">
        <div class="container">
            <div class="section-title">
                <h2>About Us</h2>
            </div>

            <div class="row content">
                <div class="col-lg-12">
                    <p>
                        We are dedicated to develop digital products and software services as a team, which consists of experienced software developers, programmers, product analysts, graphic designers and more. Tailors Manager is a software service that we developed for particularly tailoring business.
                    </p>
                </div>
            </div>
        </div>
    </section>

@endsection