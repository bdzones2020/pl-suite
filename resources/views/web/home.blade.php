@extends('web.index')

@section('title', 'Home')

@section('intro_content')

	<!-- ======= Hero Section ======= -->
    <section id="hero" class="d-flex align-items-center">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 pt-5 pt-lg-0 order-2 order-lg-1 d-flex flex-column justify-content-center">
                    <!-- <h1>Tailoring business Management & Marketing</h1> -->
                    <h1>Tailoring Business Digital Management</h1>
                    <h2>
                        A simple and comfortable solution to manage <strong class="text-dark">customers</strong>, <strong class="text-dark">orders</strong> as well as <strong class="text-dark">designs</strong>, <strong class="text-dark">payments</strong>, <strong class="text-dark">product development</strong>, <strong class="text-dark">fabrics</strong>, <strong class="text-dark">stocks</strong> and custom sale reports. 
                    </h2>
                    <!-- <div data-aos-delay="800">
                        <a href="#about" class="btn-get-started scrollto">Get Started</a>
                    </div> -->
                </div>
                <div class="col-lg-6 order-1 order-lg-2 hero-img" data-aos="fade-left">
                    <img src="{{ asset('web/assets/img/hero-image.jpeg') }}" class="img-fluid animated" alt="" />
                </div>
            </div>
        </div>
    </section>
    <!-- End Hero -->

@endsection

@section('web_content')

    <!-- ======= Features Section ======= -->
    <section id="features" class="features">
        <div class="container">
            <div class="section-title">
                <h2>Features</h2>
            </div>

            <div class="row">
                <div class="col-lg-3 col-md-4">
                    <div class="icon-box">
                        <i class="ri-bubble-chart-line" style="color: #ffbb2c;"></i>
                        <h3><a href="">Any Addons</a></h3>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 mt-4 mt-md-0">
                    <div class="icon-box">
                        <i class="ri-bar-chart-box-line" style="color: #5578ff;"></i>
                        <h3><a href="">Measuremment List</a></h3>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 mt-4 mt-md-0">
                    <div class="icon-box">
                        <i class="ri-calendar-todo-line" style="color: #e80368;"></i>
                        <h3><a href="">Design Management</a></h3>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 mt-4 mt-lg-0">
                    <div class="icon-box">
                        <i class="ri-paint-brush-line" style="color: #e361ff;"></i>
                        <h3><a href="">Fabric Dependency</a></h3>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 mt-4">
                    <div class="icon-box">
                        <i class="ri-database-2-line" style="color: #47aeff;"></i>
                        <h3><a href="">Stock Dependency</a></h3>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 mt-4">
                    <div class="icon-box">
                        <i class="ri-printer-line" style="color: #ffa76e;"></i>
                        <h3><a href="">Invoice Printing</a></h3>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 mt-4">
                    <div class="icon-box">
                        <i class="ri-file-list-3-line" style="color: #11dbcf;"></i>
                        <h3><a href="">Order Tracing</a></h3>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 mt-4">
                    <div class="icon-box">
                        <i class="ri-price-tag-2-line" style="color: #4233ff;"></i>
                        <h3><a href="">Payment Details</a></h3>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 mt-4">
                    <div class="icon-box">
                        <i class="ri-archive-line" style="color: #b2904f;"></i>
                        <h3><a href="">Stock History</a></h3>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 mt-4">
                    <div class="icon-box">
                        <i class="ri-list-check" style="color: #b20969;"></i>
                        <h3><a href="">Brand List</a></h3>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 mt-4">
                    <div class="icon-box">
                        <i class="ri-list-check-2" style="color: #ff5828;"></i>
                        <h3><a href="">Vendor List</a></h3>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 mt-4">
                    <div class="icon-box">
                        <i class="ri-global-fill" style="color: #29cc61;"></i>
                        <h3><a href="">Online Page</a></h3>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Features Section -->

    <!-- ======= More Services Section ======= -->
    <section id="more-services" class="more-services">
        <div class="container">
            <div class="row">
                <div class="col-md-6 d-flex align-items-stretch">
                    <div class="card" style="background-image: url('{{ asset('web/assets/img/more-services-1.jpg') }}');">
                        <div class="card-body">
                            <h4 class="card-title"><a href="">Designs</a></h4>
                            <p class="card-text">Create desings dynamically with measurement list and addons, store sample images and display them in your business page to get cutomers.</p>
                            <div class="read-more">
                                <a href="#"><i class="bi bi-arrow-right"></i> Read More</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 d-flex align-items-stretch mt-4 mt-md-0">
                    <div class="card" style="background-image: url('{{ asset('web/assets/img/more-services-2.jpg') }}');">
                        <div class="card-body">
                            <h4 class="card-title"><a href="">Orders</a></h4>
                            <p class="card-text">Digitally create orders with complete measurement list and addons, as well as all necessary details and calculation of cost and keep trace of product development upto delivery.</p>
                            <div class="read-more">
                                <a href="#"><i class="bi bi-arrow-right"></i> Read More</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 d-flex align-items-stretch mt-4">
                    <div class="card" style="background-image: url('{{ asset('web/assets/img/more-services-3.jpg') }}');">
                        <div class="card-body">
                            <h4 class="card-title"><a href="">Payments</a></h4>
                            <p class="card-text">Payment management in this solution helps to track each and every advance and due transactions and generate invoices dynamically to print them or share them to customers anytime.</p>
                            <div class="read-more">
                                <a href="#"><i class="bi bi-arrow-right"></i> Read More</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 d-flex align-items-stretch mt-4">
                    <div class="card" style="background-image: url('{{ asset('web/assets/img/more-services-3.jpg') }}');">
                        <div class="card-body">
                            <h4 class="card-title"><a href="">Customers</a></h4>
                            <p class="card-text">Customer list obviously will help you to make your promotion and branding better.</p>
                            <div class="read-more">
                                <a href="#"><i class="bi bi-arrow-right"></i> Read More</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 d-flex align-items-stretch mt-4">
                    <div class="card" style="background-image: url('{{ asset('web/assets/img/more-services-4.jpg') }}');">
                        <div class="card-body">
                            <h4 class="card-title"><a href="">Fabrics</a></h4>
                            <p class="card-text">Fabric is the raw metarial of this business and so this fabric management system with pricing and brands as well as other information to make the business much easier and comfortable.</p>
                            <div class="read-more">
                                <a href="#"><i class="bi bi-arrow-right"></i> Read More</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 d-flex align-items-stretch mt-4">
                    <div class="card" style="background-image: url('{{ asset('web/assets/img/more-services-4.jpg') }}');">
                        <div class="card-body">
                            <h4 class="card-title"><a href="">Stocks</a></h4>
                            <p class="card-text">Stock management of your fabrics with purchase histories and limit warnigns obviously improve you complete business productivity without any question.</p>
                            <div class="read-more">
                                <a href="#"><i class="bi bi-arrow-right"></i> Read More</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End More Services Section -->

@endsection