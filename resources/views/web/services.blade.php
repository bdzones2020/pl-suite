@extends('web.index')

@section('title', 'Services')

@section('web_content')

    <section id="services" class="services">
        <div class="container">
            <div class="section-title ">
                <h2>Services</h2>
            </div>

            <div class="row">

                <div class="col-12 d-flex align-items-stretch mb-5">
                    <div class="icon-box " >
                        <div class="icon"><i class="bx bx-layout"></i></div>
                        <h4 class="title">Design Management</h4>
                        <p class="description">
                            Tailors make dresses based on designs of particular patterns of measurements which they collect in several ways. And generally all the requirements like body measurements and additional styling parts like particular buttons, pockets etc. In this software, a tailor manager can create this documentation dynamically and more efficiently in less time. To do that there are some interesting parts like:
                        </p>
                        <ul>
                            <li>
                                <strong>Measurements</strong>: Any type of global or local or customised measurement scale name to collect the body measurements faster, store them securely and correctly and share with the tailors with tickets or online.
                            </li>
                            <li>
                                <strong>Addons</strong>: External part of a wearing product like Buttons, Pockets, Flapps, Liaise, or other design parts.
                            </li>
                            <li>
                                <strong>Images</strong>: To display samples or featured ready products digitally.
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="col-12 d-flex align-items-stretch mb-5">
                    <div class="icon-box " >
                        <div class="icon"><i class="bx bx-file"></i></div>
                        <h4 class="title">Order Management</h4>
                        <p class="description">
                            The tailor manager can easily create orders with customers' information and their particular designs, set measurements,manage addons like buttons, pockets or any other requirements, fabric amount and price, sewing cost and other relevant costs, calculation of pricing and tracks of a complete order with tickets from order to delivery and store them without any hassles for any future analysis.
                        </p>
                    </div>
                </div>

                <div class="col-12 d-flex align-items-stretch mb-5">
                    <div class="icon-box " >
                        <div class="icon"><i class="bx bx-calculator"></i></div>
                        <h4 class="title">Payment Management</h4>
                        <p class="description">
                            In tailoring business, payments are mostly parted in advance and due, and this software provides dynamic payment management with errorless calculation, payment tracking and overall history of payments. Digital invoice generator added super facilities to keep all the records up to date automatically in hard paper.
                        </p>
                    </div>
                </div>

                <div class="col-12 d-flex align-items-stretch mb-5">
                    <div class="icon-box " >
                        <div class="icon"><i class="bx bx-bar-chart-square"></i></div>
                        <h4 class="title">Fabric Management</h4>
                        <p class="description">
                            Generally professional branded tailoring centres provide fabrics and to do that they obviously keep the list of fabric with other information. Fabric management makes this much more comfortable and superbly helpful with some particular parts like:
                        </p>
                        <ul>
                            <li>
                                <strong>Brand</strong>: Independently manage brands for your fabrics to keep your information efficient and easy to track which you can also display in your home page.
                            </li>
                            <li>
                                <strong>Stock</strong>: Stock management is one of the most important parts for fabrics in the professional tailoring business. And this software makes this very comfortable, accurate, dynamically calculative and advance noticeable on stock limit.
                            </li>
                            <li>
                                <strong>Purchase History</strong>: Purchase history is basically the complete track of stocking of fabrics. This super stock tracker provides the clear information of casting for fabrics from vendors to stock.
                            </li>
                            <li>
                                <strong>Vendor</strong>: Independent vendor management to store vendor information dynamically.
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="col-12 d-flex align-items-stretch mb-5">
                    <div class="icon-box " >
                        <div class="icon"><i class="bx bx-group"></i></div>
                        <h4 class="title">Customer Management</h4>
                        <p class="description">
                            Customer list makes your communication faster and swift to inform them about their order status or any other promotional or relevant type activities dynamically through sending sms or email.
                        </p>
                    </div>
                </div>

                <div class="col-12 d-flex align-items-stretch mb-5">
                    <div class="icon-box " >
                        <div class="icon"><i class="bx bx-user"></i></div>
                        <h4 class="title">User Management</h4>
                        <p class="description">
                            To manage business, there needs to be more people in different sectors with different duties. User management with custom permission and role is an impressive part of this software.
                        </p>
                    </div>
                </div>

                <div class="col-12 d-flex align-items-stretch mb-5">
                    <div class="icon-box " >
                        <div class="icon"><i class="bx bx-detail"></i></div>
                        <h4 class="title">Reports</h4>
                        <p class="description">
                            At the end of the month or year every business needs all the important reports at once in a superbly dynamic process and this software provides this service with great comfort and meaningful statistics to analyse the whole business beautifully.
                        </p>
                    </div>
                </div>
                
            </div>
        </div>
    </section>

@endsection