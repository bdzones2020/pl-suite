@php $route = Route::current() @endphp
<header id="header" class="fixed-top d-flex align-items-center">
    <div class="container d-flex align-items-center justify-content-between">
        <div class="logo">
            <h1><a class="header-title" href="{{ route('/') }}"><img src="{{ asset('img/nityodin_logo.png') }}" alt="" class="img-fluid">{{ config('app.name') }}</a></h1>
            <!-- Uncomment below if you prefer to use an image logo -->
             <!-- <a href="{{ route('/') }}"><img src="{{ asset('img/TM-LOGO-03.png') }}" alt="" class="img-fluid"></a> -->
        </div>
        <nav id="navbar" class="navbar">
            <ul>
                <li><a class="nav-link {{ $route->getName() == 'home' ? 'active' : '' }}" href="{{ route('home') }}">Home</a></li>
                <li><a class="nav-link {{ $route->getName() == 'about-us' ? 'active' : '' }}" href="{{ route('about-us') }}">About Us</a></li>
                <li><a class="nav-link {{ $route->getName() == 'services' ? 'active' : '' }}" href="{{ route('services') }}">Services</a></li>
                <li><a class="nav-link {{ $route->getName() == 'pricing' ? 'active' : '' }}" href="{{ route('pricing') }}">Pricing</a></li>
                <!-- <li class="dropdown">
                    <a href="#"><span>Drop Down</span> <i class="bi bi-chevron-down"></i></a>
                    <ul>
                        <li><a href="#">Drop Down 1</a></li>
                        <li class="dropdown">
                            <a href="#"><span>Deep Drop Down</span> <i class="bi bi-chevron-right"></i></a>
                            <ul>
                                <li><a href="#">Deep Drop Down 1</a></li>
                                <li><a href="#">Deep Drop Down 2</a></li>
                                <li><a href="#">Deep Drop Down 3</a></li>
                                <li><a href="#">Deep Drop Down 4</a></li>
                                <li><a href="#">Deep Drop Down 5</a></li>
                            </ul>
                        </li>
                        <li><a href="#">Drop Down 2</a></li>
                        <li><a href="#">Drop Down 3</a></li>
                        <li><a href="#">Drop Down 4</a></li>
                    </ul>
                </li> -->
                <li><a class="nav-link {{ $route->getName() == 'contact' ? 'active' : '' }}" href="{{ route('contact') }}">Contact</a></li>
                @auth
                            
                    <li class="dropdown">
                        <a href="#"><span class="theme-color">{{ Auth::user()->name }}</span></a>
                        <ul>
                            @if(Auth::user()->type == 'system')

                            <li><a class="nav-link text-danger" href="{{ route('system./') }}">System</a></li>

                            @elseif(Auth::user()->type == 'owner')

                            <li><a class="nav-link text-danger" href="{{ route('dashboard./',['businessSlug' => Auth::user()->business_user->business_slug]) }}">Dashboard</a></li>

                            @endif

                            <li><a class="nav-link" href="">Profile</a></li>

                            <li><a class="nav-link {{ $route->getName() == 'home' ? 'active' : '' }}" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Logout</a>
                            </li>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </ul>
                    </li>

                @else

                    <li><a class="nav-link scrollto" href="{{ route('login') }}">Login</a></li>
                    <li><a class="nav-link scrollto" href="{{ route('register') }}">Register</a></li>

                @endauth
            </ul>
            <i class="bi bi-list mobile-nav-toggle"></i>
        </nav>
    </div>
</header>