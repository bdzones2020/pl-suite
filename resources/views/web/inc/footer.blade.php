<style type="text/css" media="screen">
    .cursor-pointer {cursor: pointer;}
    .cursor-pointer:hover {text-decoration: underline;}
</style>
<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-12 text-left">
                <div class="">
                    <h4><a class="header-title" href="{{ route('/') }}">
                        <!-- <img src="{{ asset('img/TM-LOGO-02.png') }}" alt="" class="img-fluid"> -->
                        <strong>{{ config('app.name') }}</strong></a></h4>
                    <!-- Uncomment below if you prefer to use an image logo -->
                     <!-- <a href="{{ route('/') }}"><img src="{{ asset('img/TM-LOGO-03.png') }}" alt="" class="img-fluid"></a> -->
                </div>
                <hr class="short-line"></hr>
                <address>
                    <div class="col-12 py-1">
                        <i class="ri-map-pin-line"></i> {{ config('app.address') }}
                    </div>
                    <div class="col-12 py-1">
                        <i class="ri-mail-send-line"></i> {{ config('app.email') }}
                    </div>
                    <div class="col-12 py-1">
                        <i class="ri-phone-line"></i> {{ config('app.contact_no') }}
                    </div>
                </address>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-12">
                <div class="row">
                    <div class="col-12 my-1">
                        <h4><span>Important Links</span></h4>
                    </div>
                    <hr class="short-line"></hr>
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="row">
                            <div class="col-12 py-1">
                                <a href="{{ route('terms-&-conditions') }}" class="cursor-pointer text-dark">Terms & Conditions</a>
                            </div>
                            <div class="col-12 py-1">
                                <a href="{{ route('privacy-policy') }}" class="cursor-pointer text-dark">Privacy Policy</a>
                            </div>
                            <div class="col-12 py-1">
                                <a href="{{ route('faq') }}" class="cursor-pointer text-dark">Faq</a>
                            </div>
                            <!-- <div class="col-12 py-1">
                                <a href="{{ route('services') }}" class="cursor-pointer text-dark">Services</a>
                            </div>
                            <div class="col-12 py-1">
                                <a href="{{ route('pricing') }}" class="cursor-pointer text-dark">Pricing</a>
                            </div>
                            <div class="col-12 py-1">
                                <a href="{{ route('about-us') }}" class="cursor-pointer text-dark">About Us</a>
                            </div> -->
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 text-center contact">
                        <div class="col-12 mb-3">
                            <h6>Other Platforms</h6>
                        </div>
                        <div class="social-links">
                            <a href="#" class="twitter"><i class="bi bi-twitter"></i></a>
                            <a href="#" class="facebook"><i class="bi bi-facebook"></i></a>
                            <a href="#" class="instagram"><i class="bi bi-instagram"></i></a>
                            <a href="#" class="linkedin"><i class="bi bi-linkedin"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <hr>

        <div class="row d-flex align-items-center">
            <div class="col-12 text-center">
                <div class="copyright">&copy; Copyright <strong>{{ config('app.name') }}</strong>. All Rights Reserved by <strong class="text-dark">BDZONES</strong> </div>
                <div class="credits">
                    <!-- All the links in the footer should remain intact. -->
                    <!-- You can delete the links only if you purchased the pro version. -->
                    <!-- Licensing information: https://bootstrapmade.com/license/ -->
                    <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/vesperr-free-bootstrap-template/ -->
                    Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
                </div>
            </div>
        </div>
    </div>
</footer>