@extends('web.index')

@section('title', 'Contact')

@section('web_content')

    <section id="contact" class="contact">
        <div class="container">
            <div class="section-title">
                <h2>Contact Us</h2>
            </div>

            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <div class="contact-about">
                        <h3 class="">
                            <!-- <span class="logo">
                                <img src="{{ asset('img/TM-LOGO-03.png') }}" alt="" class="img-fluid">
                            </span> -->
                            {{ config('app.name') }}
                        </h3>
                        <!-- <hr class="short-line"> -->
                        <p>A digital management system for commercial or personal tailoring business.</p>
                    </div>
                    <div class="info">
                        <div>
                            <i class="ri-map-pin-line"></i>
                            <p>
                                {{ config('app.address') }}
                            </p>
                        </div>

                        <div>
                            <i class="ri-mail-send-line"></i>
                            <p>
                                {{ config('app.email') }}
                            </p>
                        </div>

                        <div>
                            <i class="ri-phone-line"></i>
                            <p>
                                {{ config('app.contact_no') }}
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6">
                    <div class="col-12 mb-3 text-center">
                        <h5>Have a query? please write to us</h5>
                    </div>
                    <hr class="short-line"></hr>
                    <form action="{{ route('send-message') }}" method="post" role="form" class="php-email-form">
                        @csrf
                        <div class="form-group">
                            <input type="text" name="name" class="form-control" id="name" value="{{ old('name') }}" placeholder="Your Name" required />
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control" name="email" id="email" value="{{ old('email') }}" placeholder="Your Email" required />
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control quantity_class" name="contact_number" id="contact_no" value="{{ old('contact_number') }}" minlength="11" maxlength="11" placeholder="Your Contact Number" pattern="^(?:01)?\d{11}$" onkeyup="this.value=this.value.replace(/[^0-9:]/g,'');" required="" />
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="subject" id="subject" value="{{ old('subject') }}" placeholder="Subject" required />
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" name="message" rows="3" placeholder="Your Message" required>{{ old('message') }}</textarea>
                        </div>
                        <div class="my-3">
                            <div class="loading">Loading</div>
                            <div class="error-message"></div>
                            <div class="sent-message">Your message has been sent. Thank you!</div>
                        </div>
                        <div class="text-center"><button type="submit">Send Message</button></div>
                    </form>
                </div>

                <div class="col-lg-4 col-md-12">
                    <div class="col-12 mb-3 text-center">
                        <h5>Find us here</h5>
                    </div>
                    <hr class="short-line"></hr>
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3650.4006066854786!2d90.36263671551282!3d23.804349584562928!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755c0e6f3614347%3A0x8b4484c15f761888!2sBDZONES!5e0!3m2!1sen!2sbd!4v1640943174489!5m2!1sen!2sbd" width="100%" height="287" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                </div>
            </div>
        </div>
    </section>

@endsection

@push('scripts')

    <script type="text/javascript">
        $(document).ready(function(){
            //
        });
    </script>

@endpush