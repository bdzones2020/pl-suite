<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8" />
        <meta content="width=device-width, initial-scale=1.0" name="viewport" />

        <title>{{ config('app.name') }} @yield('title')</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="keywords" content="Tailors Manager">
        <meta name="description" content="Tailors Manager">
        <meta name="author" content="bdzones.net">

        <!-- Favicons -->
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('img/nityodin_logo.png') }}" />

        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet" />

        <!-- Font-awesome 4.7.0 -->
        <!-- <link href="{{ asset('font-awesome-4.7.0/css/font-awesome.min.css') }}" rel="stylesheet" /> -->

        <!-- Vendor CSS Files -->
        <link href="{{ asset('web/assets/vendor/aos/aos.css') }}" rel="stylesheet" />
        <link href="{{ asset('web/assets/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" />
        <link href="{{ asset('web/assets/vendor/bootstrap-icons/bootstrap-icons.css') }}" rel="stylesheet" />
        <link href="{{ asset('web/assets/vendor/boxicons/css/boxicons.min.css') }}" rel="stylesheet" />
        <link href="{{ asset('web/assets/vendor/glightbox/css/glightbox.min.css') }}" rel="stylesheet" />
        <link href="{{ asset('web/assets/vendor/remixicon/remixicon.css') }}" rel="stylesheet" />
        <link href="{{ asset('web/assets/vendor/swiper/swiper-bundle.min.css') }}" rel="stylesheet" />

        <!-- Template Main CSS File -->
        <link href="{{ asset('web/assets/css/style.css') }}" rel="stylesheet" />
        <link href="{{ asset('web/assets/css/custom.css') }}" rel="stylesheet" />

        @stack('styles')

    </head>

    <body>
        
        @include('web.inc.header')

        <main id="main" class="mt-5">

            @yield('intro_content')

            @yield('web_content')
            
        </main>
        <!-- End #main -->

        @include('web.inc.footer')

        <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

        <script type="text/javascript" src="{{ asset('js/jquery-3.5.1.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/jquery-migrate-3.0.0.min.js') }}"></script>

        <!-- Vendor JS Files -->
        <script src="{{ asset('web/assets/vendor/purecounter/purecounter.js') }}"></script>
        <script src="{{ asset('web/assets/vendor/aos/aos.js') }}"></script>
        <script src="{{ asset('web/assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
        <script src="{{ asset('web/assets/vendor/glightbox/js/glightbox.min.js') }}"></script>
        <script src="{{ asset('web/assets/vendor/isotope-layout/isotope.pkgd.min.js') }}"></script>
        <script src="{{ asset('web/assets/vendor/swiper/swiper-bundle.min.js') }}"></script>
        <script src="{{ asset('web/assets/vendor/php-email-form/validate.js') }}"></script>

        <!-- Template Main JS File -->
        <script src="{{ asset('web/assets/js/main.js') }}"></script>

        <script type="text/javascript">
            $(document).ready(function(){
                $('[data-toggle="tooltip"]').tooltip({
                    trigger : 'hover',
                    boundary: 'window'
                });
                $('.quantity_class').attr('onkeyup',"this.value=this.value.replace(/[^0-9:]/g,'');");
            });
        </script>

        @stack('scripts')

    </body>
</html>