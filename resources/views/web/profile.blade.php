@extends('web.index')

@section('web_content')

    <style type="text/css" media="screen">
        .hide {display: none;}
    </style>

    <section id="contact" class="contact">
        <div class="container">
            <div class="section-title">
                <h2>Contact Us</h2>
            </div>

            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <div class="contact-about">
                        <h3 class="">
                            <!-- <span class="logo">
                                <img src="{{ asset('img/TM-LOGO-03.png') }}" alt="" class="img-fluid">
                            </span> -->
                            {{ config('app.name') }}
                        </h3>
                        <hr class="short-line">
                        <p>Cras fermentum odio eu feugiat. Justo eget magna fermentum iaculis eu non diam phasellus. Scelerisque felis imperdiet proin fermentum leo. Amet volutpat consequat mauris nunc congue.</p>
                        <div class="social-links">
                            <a href="#" class="twitter"><i class="bi bi-twitter"></i></a>
                            <a href="#" class="facebook"><i class="bi bi-facebook"></i></a>
                            <a href="#" class="instagram"><i class="bi bi-instagram"></i></a>
                            <a href="#" class="linkedin"><i class="bi bi-linkedin"></i></a>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 mt-4 mt-md-0">
                    <div class="info">
                        <div>
                            <i class="ri-map-pin-line"></i>
                            <p>
                                A108 Adam Street<br />
                                New York, NY 535022
                            </p>
                        </div>

                        <div>
                            <i class="ri-mail-send-line"></i>
                            <p>info@example.com</p>
                        </div>

                        <div>
                            <i class="ri-phone-line"></i>
                            <p>+1 5589 55488 55s</p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-5 col-md-12">
                    <form action="forms/contact.php" method="post" role="form" class="php-email-form">
                        <div class="form-group">
                            <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" required />
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" required />
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" required />
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" name="message" rows="5" placeholder="Message" required></textarea>
                        </div>
                        <div class="my-3">
                            <div class="loading">Loading</div>
                            <div class="error-message"></div>
                            <div class="sent-message">Your message has been sent. Thank you!</div>
                        </div>
                        <div class="text-center"><button type="submit">Send Message</button></div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <section id="contact" class="contact section-bg">
        <div class="container">
            <div class="section-title">
                <h2 class="fw-bold">User Profile <button class="btn btn-sm btn-outline-dark editBtn">Edit</button></h2>
            </div>

            <div class="row">
                <div class="col-lg-4 col-md-8 col-sm-12 offset-lg-4 offset-md-2">
                    @if(Session::has('message_success'))
                        <div class="alert alert-success alert-dismissible d-alert">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <strong>{{ Session::get('message_success') }}</strong>
                        </div>
                    @endif

                    @if(Session::has('message_warning'))
                        <div class="alert alert-warning alert-dismissible d-alert">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <strong>{{ Session::get('message_warning') }}</strong>
                        </div>
                    @endif
                </div>
            </div>

            <div class="row">
                <div class="col-lg-4 col-md-8 col-sm-12 offset-lg-4 offset-md-2">
                    <form id="profileUpdateForm" action="{{ route('profile.update') }}" method="post" role="form" class="php-email-form">
                        @csrf

                        <div class="row mb-3 form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-sm-12 control-label">User Name</label>
                            <div class="col-sm-12">
                                <input id="name" type="text" class="form-control" name="name" value="{{ !empty($profile) ? $profile->name : old('name') }}" placeholder="User Name" required="true" readonly="">
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="row mb-3 form-group{{ $errors->has('contact_no') ? ' has-error' : '' }}">
                            <label for="contact_no" class="col-sm-12 control-label">Contact Number</label>
                            <div class="col-sm-12">
                                <input id="contact_no" type="tel" class="form-control quantity_class" name="contact_no" value="{{ !empty($profile) ? $profile->contact_no : old('contact_no') }}" minlength="11" maxlength="11" placeholder="01XXXXXXXXX" pattern="^(?:01)?\d{11}$" required="" readonly="">
                                @if ($errors->has('contact_no'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('contact_no') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="row mb-3 form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-sm-12 control-label">Email <small class="text-success">(optional)</small></label>
                            <div class="col-sm-12">
                                <input id="email" type="email" class="form-control" name="email" value="{{ !empty($profile) ? $profile->email : old('email') }}" placeholder="user@mail.com" readonly="">
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="row mb-2 update-password-div hide form-group">
                            <div class="col-12">
                                <div class="form-check form-switch">
                                    <input class="form-check-input" type="checkbox" id="update_password" name="update_password" />
                                    <label class="form-check-label" for="update_password">Update Password</label>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-3 password-div hide form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-sm-12 control-label">Password</label>
                            <div class="col-sm-12">
                                <input placeholder="Type password" id="password" type="password" class="form-control password-input @error('password') is-invalid @enderror" name="password" {{ !empty($profile) ? 'disabled' : 'required' }} />
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="row mb-3 form-group password-div hide">
                            <label for="password-confirm" class="col-sm-12 control-label">Confirm Password</label>
                            <div class="col-sm-12">
                                <input placeholder="Retype password" id="password-confirm" type="password" class="form-control password-input @error('password') is-invalid @enderror" name="password_confirmation" {{ !empty($profile) ? 'disabled' : 'required' }} />
                            </div>
                        </div>  

                        <div class="row mb-3 form-group">
                            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center">
                                <div>
                                    <button type="button" class="btn btn-danger px-4 hide">
                                        Cancel
                                    </button>
                                </div>
                                <div>
                                    <button type="submit" class="btn btn-tertiary px-4 hide" disabled="">
                                        Update
                                    </button>
                                </div>
                            </div>
                        </div>
                        
                    </form>
                </div>
            </div>
        </div>
    </section>
    <!-- End Contact Us Section -->

@endsection

@push('scripts')

    <script type="text/javascript">
        $(document).ready(function(){
            $('#update_password').change(function(){
                if($(this).prop('checked') == true)
                {
                    $('.password-div').removeClass('hide');
                    $('.password-input').prop('disabled',false);
                    $('.password-input').prop('required',true);
                }else{
                    $('.password-div').addClass('hide');
                    $('.password-input').prop('disabled',true);
                    $('.password-input').prop('required',false);
                }
            });

            $('.editBtn').click(function(){
                $('#profileUpdateForm').find('.form-control').prop('readonly',false);
                $('.update-password-div').removeClass('hide');
            });

            $('.alert').fadeOut(3000);
        });
    </script>

@endpush