@extends('web.index')

@section('title', 'Forgot Password')

@section('web_content')

    <section id="registration">
        <div class="container">
            <div class="row justify-content-center form-bg-image">
                <div class="col-lg-6 offset-lg-0 d-flex align-items-center justify-content-center">
                    <div class="signin-inner my-3 my-lg-0 bg-white shadow-soft border rounded border-light p-4 p-lg-5 w-100 fmxw-500">
                        <h1 class="h3">Forgot your password?</h1>
                        <p class="mb-4">Don't fret! Just type in your email and we will send you a code to reset your password!</p>
                        <form method="POST" action="{{ route('password.email') }}">
                            @csrf
                            <div class="mb-4">
                                <label for="email">Your Email</label>
                                <div class="input-group">

                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="your registered email" required autocomplete="off" autofocus>

                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>  
                            </div>
                            <div class="d-grid">
                                <button type="submit" class="btn btn-dark">{{ __('Send Password Reset Link') }}</button>
                            </div>
                        </form>
                        <div class="d-flex justify-content-center align-items-center mt-4">
                            <span class="fw-normal">
                                Have you recalled?
                                <a href="{{ route('login') }}" class="fw-bold">Sign in</a>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@push('scripts')

    <script type="text/javascript">
        $(document).ready(function(){
            //
        });
    </script>

@endpush
