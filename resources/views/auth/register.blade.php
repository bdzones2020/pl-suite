@extends('web.index')

@section('title', 'Register')

@section('web_content')

    <section id="registration">
        <div class="container">
            <div class="section-title">
                <h2 class="fw-bold">Registration to {{ config('app.name') }}</h2>
            </div>

            <form action="{{ route('register') }}" method="post" role="form" class="">
                @csrf

                <input type="hidden" name="gender" id="gender" value="">

                <div class="row justify-content-center mb-3">
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" id="registration_type">

                                        <label class="form-check-label text-primary" for="registration_type">
                                            Register as Business Owner
                                        </label>
                                    </div>
                                </div>
                                <div id="businessNameInput" class="form-group mt-3 hide">
                                    <label for="business_name" class="form-label text-dark">Business Name <span class=" text-danger">*</span> <small class="text-danger">( Max Length: 50 characters )</small></label>
                                    <input id="business_name" type="text" class="form-control @error('business_name') is-invalid @enderror" name="business_name" value="{{ old('business_name') }}" placeholder="Business Name" autocomplete="off" />

                                    @error('business_name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                    <input type="hidden" id="type" name="type" value="user">
                                </div>
                            </div>
                        </div>                                
                    </div>
                </div>

                <div class="row">

                    <div class="col-md-6">

                        <div class="form-group mb-3">
                            <label for="name" class="form-label text-dark">User Name <span class=" text-danger">*</span> <small class="text-danger">( Max Length: 50 characters )</small></label>
                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" placeholder="User Name" required autofocus autocomplete="off" />

                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group mb-3">
                            <label for="contact_no" class="form-label text-dark">Contact Number <span class=" text-danger">*</span> <small class="text-danger">( Length: 11 digits )</small></label>
                            <input id="contact_no" type="tel" class="form-control @error('contact_no') is-invalid @enderror" name="contact_no" value="{{ old('contact_no') }}" minlength="11" maxlength="11" placeholder="01XXXXXXXXX" pattern="^(?:01)?\d{11}$" autocomplete="off" required="" />
                            @error('contact_no')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group mb-3">
                            <label for="email" class="form-label text-dark">Email <span class=" text-danger">*</span> <small class="text-danger">( Max Length: 50 characters )</small></label>
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}"  placeholder="your@mail.com" autocomplete="off" required />
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                    </div>

                    <div class="col-md-6">

                        <div class="form-group mb-3">
                            <label for="password" class="form-label text-dark">Password <span class=" text-danger">*</span> <small class="text-danger">( Min Length: 8 characters )</small></label>
                            <input placeholder="Type password" id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" autocomplete="off" required />
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group mb-3">
                            <label for="password-confirm" class="form-label text-dark">Confirm Password <span class=" text-danger">*</span></label>
                            <input placeholder="Retype password" id="password-confirm" type="password" class="form-control @error('password') is-invalid @enderror" name="password_confirmation" required>
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group mb-3">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="terms&conditions" id="terms&conditions" required>

                                <label class="form-check-label" for="terms&conditions">
                                    I agree to the <a href="{{ route('terms-&-conditions') }}" class="link">Terms and Conditions</a>
                                </label>
                            </div>
                        </div>

                        <div class="form-group mb-3">
                            <button type="submit" class="btn btn-outline-dark w-100">Register</button>
                        </div>

                    </div>

                </div>

            </form>

            <div class="col-md-12 mt-3">
                <div class="fw-normal text-center">
                    Already have an account?
                    <a href="{{ route('login') }}" class="fw-bold link">Login here</a>
                </div>
            </div>
        </div>
    </section>

@endsection

@push('scripts')

    <script type="text/javascript">
        $(document).ready(function(){
            $('#registration_type').change(function(){
                checkType();
            });

            function checkType()
            {
                if($('#registration_type').prop('checked'))
                {
                    $('#businessNameInput').show();
                    $('#business_name').prop('required',true);
                    $('#type').val('owner');
                }else{
                    $('#businessNameInput').hide();
                    $('#business_name').prop('required',false);
                    $('#type').val('user');
                }
            }

            checkType()

            $('#name').change(function(){
                $(this).val().length > 30 ? $('#nameAlert').removeClass('hide') : $('#nameAlert').addClass('hide') ;
            });
            $('#email').change(function(){
                $(this).val().length > 30 ? $('#emailAlert').removeClass('hide') : $('#emailAlert').addClass('hide') ;
            });
            $('#contact_no').change(function(){
                $(this).val().length !== 11 ? $('#contactAlert').removeClass('hide') : $('#contactAlert').addClass('hide') ;
            });
            $('#business_name').change(function(){
                $(this).val().length > 50 ? $('#business_nameAlert').removeClass('hide') : $('#business_nameAlert').addClass('hide') ;
            });
        });
    </script>

@endpush
