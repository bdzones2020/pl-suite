@extends('web.index')

@section('title', 'Login')

@section('web_content')

    <section id="login">
        <div class="container">
            <div class="section-title">
                <h2 class="fw-bold">Login to {{ config('app.name') }}</h2>
            </div>

            <form action="{{ route('login') }}" method="post" role="form" class="php-email-form">
                @csrf

                <input type="hidden" name="gender" id="gender" value="">

                <div class="row">

                    <div class="col-lg-4 offset-lg-4">

                        <div class="form-group mb-3">
                            <label for="email" class="form-label text-dark">Email or Contact Number</label>
                            <input id="email" type="text" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}"  placeholder="your@email.com or 01xxxxxxxxx" autocomplete="off" />
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group mb-3">
                            <label for="password" class="form-label text-dark">Password</label>
                            <input placeholder="Type password" id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" autocomplete="off" required />
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="d-flex justify-content-between align-items-top mb-3">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                <label class="form-check-label" for="remember">
                                    {{ __('Remember Me') }}
                                </label>
                            </div>

                            @if (Route::has('password.request'))

                                <div><a href="{{ route('password.request') }}" class="small text-right">Forgot Password?</a></div>

                            @endif
                        </div>

                        <button type="submit" class="btn btn-outline-dark w-100">Login</button>

                    </div>

                </div>

            </form>

            <div class="col-md-12 mt-3">
                <div class="fw-normal text-center">
                    Not registered yet?
                    <a href="{{ route('register') }}" class="fw-bold link">Register here</a>
                </div>
            </div>
        </div>
    </section>    

@endsection