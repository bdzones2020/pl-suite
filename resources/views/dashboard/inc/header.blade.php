@php $route = Route::current() @endphp
<nav class="navbar navbar-expand-md fixed-top bg-white">
    <div class="container-fluid">
        <a href="#menu-toggle" class="btn btn-sm btn-outline-primary" id="menu-toggle">
            <i class="fa fa-angle-double-left fa-2x"></i>
        </a>
        
        <a class="navbar-brand" href="">
            <!-- <img class="logo" src="{{ asset('img/nityodin_logo.png') }}"> -->
            {{ config('app.name') }}
        </a>
        
        <button id="aside-toggler" class="btn btn-outline-primary" type="button">
            <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="collapsibleNavbar">
            <ul id="collapsibleNavbarLinks" class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="#">Link 1</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Link 2</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<div class="toparea theme-bg text-secondary animated">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-9 col-sm-8 col-7 toparea-btn-box">
                <a href="{{ route('home') }}" class="btn-icon px-2 py-1">
                    <i class="fa fa-home"></i> <span>Home</span>
                </a>
                <a href="{{ route('home') }}" class="btn-icon px-2 py-1">
                    <i class="fa fa-question-circle-o"></i> <span>FAQ</span>
                </a>
            </div>
            <div class="col-md-3 col-sm-4 col-5">
                
                @auth
                    <div class="dropdown pull-right">
                        <a href="javascript:void(0);" id="topareaProfile" class="dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-user-circle-o"></i>
                            {{ Auth::user()->name }}
                        </a>
                        <div class="dropdown-menu" aria-labelledby="topareaProfile">

                            @if(Auth::user()->type == 'system')

                            <a href="{{ route('system./') }}" class="dropdown-item text-dark"><i class="fa fa-dashboard"></i> System</a>

                            @endif

                            <a href="" class="dropdown-item"><i class="fa fa-user"></i> Profile</a>

                            <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();" class="dropdown-item"><i class="fa fa-sign-out"></i> Logout</a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>

                            <div class="dropdown-divider"></div>

                        </div>
                    </div>
                @else
                    <span class="pull-right"><a href="{{ route('login') }}">Login</a></span>
                    <span class="pull-right mr-2"><a href="{{ route('register') }}">Register</a></span>
                @endauth

            </div>
        </div>
    </div>
</div>