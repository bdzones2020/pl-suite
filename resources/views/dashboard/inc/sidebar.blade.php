@php $route = Route::current() @endphp
<div id="sidebar-wrapper" class="theme-bg">
    <ul class="sidebar-nav theme-bg">
        <li class="list-group-item">
            <a class="" href="">
                <i class="fa fa-dashboard"></i> Dashboard
            </a>
        </li>
        <li class="list-group-item">
            <a class="" href="">
                <i class="fa fa-file-excel-o"></i> Orders
            </a>
        </li>
        <li class="list-group-item">
            <a class="" href="">
                <i class="fa fa-cubes"></i> Products
            </a>
        </li>
        <li class="list-group-item">
            <a class="" href="">
                <i class="fa fa-database"></i> Stocks
            </a>
        </li>
        <li class="list-group-item">
            <a class="" href="">
                <i class="fa fa-glass"></i> Discounts
            </a>
        </li>
        <li class="list-group-item">
            <a class="" href="">
                <i class="fa fa-magnet"></i> Coupons
            </a>
        </li>
        <li class="list-group-item">
            <a class="" href="">
                <i class="fa fa-file-excel-o"></i> Reports
            </a>
        </li>
        <li class="list-group-item">
            <a class="" href=""><i class="fa fa-star"></i> Brands</a>
        </li>
        <li class="list-group-item">
            <a data-toggle="collapse" href="#submenuSettings" type="button">
                <i class="fa fa-wrench"></i> Settings
            </a>
            <ul class="list-group collapse {{ strpos($route->getName(), 'dashboard.settings') !== false || strpos($route->getName(), 'dashboard.info') !== false ? 'show' : '' }}" id="submenuSettings">
                <li class="list-group-item">
                    <a href="" class="{{ $route->getName() == 'dashboard.info' ? 'active' : '' }}"><i class="fa fa-registered"></i> Business Info</a>
                </li>
                <li class="list-group-item">
                    <a href="" class="{{ $route->getName() == 'dashboard.settings' ? 'active' : '' }}"><i class="fa fa-wrench"></i> Business Settings</a>
                </li>
            </ul>
        </li>
    </ul>
</div>
