<!DOCTYPE html>
<html dir="ltr" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="keywords" content="{{ !empty($appInfo->name) ? $appInfo->name : config('app.name') }}, Local Business Comminication & Management System, @yield('meta_keywords')">
        <meta name="description" content="{{ !empty($appInfo->name) ? $appInfo->name : config('app.name') }}, Local Business Comminication & Management System, @yield('meta_description')">
        <meta name="author" content="mosharof.bdzones@gmail.com">

        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('img/nityodin_logo.png') }}">

        <!-- title -->
        <title>@yield('title', !empty($appInfo->name) ? $appInfo->name : config('app.name'))</title>

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- Styles -->
        <link href="{{ asset('board/css/bootstrap.min.css') }}" rel="stylesheet">

        <!-- select2 -->
        <link href="{{ asset('plugins/select2/select2.min.css') }}" rel="stylesheet" />

        <!-- Template Main CSS File -->
        <link href="{{ asset('board/css/animate.css') }}" rel="stylesheet" />
        <link href="{{ asset('board/css/ui.css') }}" rel="stylesheet" />
        <link href="{{ asset('board/css/responsive.css') }}" rel="stylesheet" />
        <link href="{{ asset('board/css/layouts.css') }}" rel="stylesheet" />
        <link href="{{ asset('board/css/custom.css') }}" rel="stylesheet" />

        <!-- font awesome -->
        <link href="{{ asset('font-awesome-4.7.0/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">

        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;500;600&display=swap" rel="stylesheet">
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@300;400&display=swap" rel="stylesheet"> 

        @stack('styles')

    </head>

    <body>
        
        <div id="app">

            @include('dashboard.inc.header')

            <div id="wrapper">

                @include('dashboard.inc.sidebar')

                <div id="aside-wrapper" class="theme-bg">
                    <ul class="aside-nav">
                        <li class="list-group-item">
                            <a href="{{ route('home') }}" target="_blank">
                                <i class="fa fa-home"></i> Nityodin
                            </a>
                        </li>
                    </ul>
                </div>

                <!-- Page Content -->
                <div id="page-content-wrapper">

                    <div class="page-content">
                        
                        @yield('dashboard_content')

                    </div>

                    @include('dashboard.inc.footer')

                </div>
                <!-- /#page-content-wrapper -->

            </div>

            <div class="modal fade" id="searchModal">
                <div class="modal-dialog modal-md">
                    <div class="modal-content">
                        <form action="header_submit" method="get" accept-charset="utf-8">
                            <div class="modal-body">
                                    
                                <form id="search-form-modal" action="#" class="">
                                    <div class="form-group mb-2">
                                        <select name="search_type" class="form-control">
                                            <option value="product">Product</option>
                                            <option value="service">Service</option>
                                            <option value="shop">Shop</option>
                                        </select>
                                    </div>
                                    <div class="form-group mb-2">
                                        <input type="text" class="form-control" name="search_for" placeholder="Search here ...">
                                    </div>
                                    <button class="btn btn-primary btn-block">Search</button>
                                </form>
                                
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="loginModal">
                <div class="modal-dialog modal-md">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="text-center">
                                <h4>User Login</h4>
                            </div>
                            <hr>
                            <form method="POST" action="{{ route('login') }}">
                                @csrf
                                <input type="hidden" id="backRoute" name="backRoute" value="">
                                <!-- <a href="#" class="btn btn-facebook btn-block mb-2"> <i class="fa fa-facebook-f"></i> &nbsp Sign in
                                    with
                                    Facebook</a>
                                <a href="#" class="btn btn-google btn-block mb-4"> <i class="fa fa-google"></i> &nbsp Sign in with
                                    Google</a> -->
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <input id="email" type="text" class="form-control @error('email') is-invalid @enderror"
                                        name="email" value="{{ old('email') }}" placeholder="registered email or contact number" required autocomplete="email" autofocus>

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="secret password" required autocomplete="current-password">

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    @if (Route::has('password.request'))
                                        <a class="float-right" href="{{ route('password.request') }}">
                                            {{ __('Forgot Password?') }}
                                        </a>
                                    @endif
                                    <label class="float-left custom-control custom-checkbox"> <input type="checkbox"
                                            class="custom-control-input" checked="" id="remember"
                                            {{ old('remember') ? 'checked' : '' }}>
                                        <div class="custom-control-label"> {{ __('Remember Me') }} </div>
                                    </label>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-block"> Login </button>
                                </div>
                            </form>
                            <hr>
                            <p class="text-center mt-4">Don't have account? <a href="{{ route('register') }}">Sign up</a></p>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <!-- Scripts -->
        <script src="{{ asset('board/js/jquery-3.5.1.min.js') }}"></script>
        <script src="{{ asset('board/js/jquery-migrate-3.0.0.min.js') }}"></script>
        <script src="{{ asset('board/js/popper.min.js') }}"></script>
        <script src="{{ asset('board/js/bootstrap-4.5.2.min.js') }}"></script>
        <script src="{{ asset('plugins/select2/select2.min.js') }}"></script>

        <script type="text/javascript">
            $(document).ready(function(){
                $('[data-toggle="tooltip"]').tooltip({
                    trigger : 'hover'
                });
                // remove char from numaric type text field
                $('.quantity_class').attr('onkeyup',"this.value=this.value.replace(/[^0-9:]/g,'');");

                $("#menu-toggle").click(function(e) {
                    e.preventDefault();
                    $("#aside-wrapper").removeClass("toggled");
                    $("#wrapper").toggleClass("toggled");
                    $('#navbarSupportedContent').collapse('hide');
                });
                $("#aside-toggler").click(function(e) {
                    e.preventDefault();
                    $("#wrapper").removeClass('toggled');
                    $("#aside-wrapper").toggleClass("toggled");
                });
                $('#navbar-toggler').click(function(){
                    $("#wrapper").removeClass('toggled');
                    $('#navbarSupportedContent').collapse('toggle');
                });
                $('#page-content-wrapper, .toparea, .toparea .dropdown a').click(function(){
                    $("#wrapper").removeClass("toggled");
                    $("#aside-wrapper").removeClass("toggled");
                    $('#navbarSupportedContent').collapse('hide');
                });

                $('#search-btn').click(function(){
                    $('#searchModal').modal('show');
                });

                $('#loginModal').on('show.bs.modal', function () {
                    $('#backRoute').val(window.location.href);
                });

                $('#loginModal').on('hide.bs.modal', function () {
                    $('#backRoute').val('');
                });
            });
        </script>

        @stack('scripts')

    </body>
</html>