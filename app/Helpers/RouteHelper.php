<?php

namespace App\Helpers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;

class RouteHelper
{
    public function getDashboardRoutes($model = null)
    {
        $routeList = $this->allRouteList('dashboard');

        $array = [];

        foreach($routeList as $r => $routeString)
        {
            if(!strpos($routeString,'profile') && !strpos($routeString,'help-guide'))
            {
                if($routeString != 'dashboard./')
                {
                    $routeArray = explode('.', $routeString);
                    $routeAction = [];

                    foreach($routeList as $checkRoute)
                    {
                        if($routeArray[1] == explode('.', $checkRoute)[1])
                        {
                            $routeAction[] = str_replace('dashboard.', '', $checkRoute);
                        }
                    }
                    $array[ucwords(str_replace('-',' ',$routeArray[1]))] = $routeAction;
                }
            }
        }

        return $array;
    }

    public function getSystemRoutes()
    {
        $routeList = $this->allRouteList('system');

        $array = [];

        foreach($routeList as $r => $routeString)
        {
            if($routeString != 'system./')
            {
                $routeArray = explode('.', $routeString);
                $routeAction = [];

                foreach($routeList as $checkRoute)
                {
                    if($routeArray[1] == explode('.', $checkRoute)[1])
                    {
                        $routeAction[] = str_replace('system.', '', $checkRoute);
                    }
                }
                $array[ucwords(str_replace('-',' ',$routeArray[1]))] = $routeAction;
            }
        }

        return $array;
    }

    protected function allRouteList($groupName = '')
    {
        $list = \Route::getRoutes()->getRoutesByName();
        if (empty($groupName)) {
            return $list;
        }

        $routes = [];
        foreach ($list as $name => $route) {
            if (Str::startsWith($name, $groupName)) {
                $routes[$name] = $route->getName();
            }
        }

        return $routes;
    }

    public function setPermission($request)
    {
        if(!empty($request->input('all_permissions')))
        {
            $setPermission = 'dashboard,all';
        }else{
            $permissions = '';
            foreach($request->access as $routeName => $value)
            {
                if($value == 'yes')
                {
                    $permissions .= ','.$routeName;
                }
            }
            $setPermission = 'dashboard,profile,help-guide'.$permissions;
        }
        return $setPermission;
    }
}