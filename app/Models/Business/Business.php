<?php

namespace App\Models\Business;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\User;
use App\Models\Business\BusinessUser;
use App\Models\Business\BusinessSettings;
use App\Models\Business\BusinessSubscription;

class Business extends Model
{
    use HasFactory;

    protected $fillable = ['user_id','business_name','business_slug','business_logo','business_type','business_code','business_contact_numbers','business_email','business_address','business_rating','business_status','subscribed_package','package_weight','package_status','max_users','valid_till'];

    public function user()
    {
        return $this->belongsTo(User::class)->select('id','name');
    }

    public function business_users()
    {
        return $this->hasMany(BusinessUser::class)->select('id','user_id','business_id','business_user_role_id','status');
    }

    public function business_settings()
    {
        return $this->hasOne(BusinessSettings::class);
    }

    public function business_subscriptions()
    {
        return $this->hasMany(BusinessSubscription::class);
    }
}
