<?php

namespace App\Models\Business;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\User;
use App\Models\Business\Business;
use App\Models\Business\BusinessSettings;
use App\Models\Business\BusinessUserRole;

class BusinessUser extends Model
{
    use HasFactory;

    protected $fillable = ['business_id','user_id','business_slug','business_user_role_id','status'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function business_user_role()
    {
        return $this->belongsTo(BusinessUserRole::class);
    }

    public function business()
    {
        return $this->belongsTo(Business::class)->select('id','user_id','business_name','business_slug','business_logo','business_contact_numbers','business_email','business_address','business_code','subscribed_package','package_weight','package_status','max_users','valid_till');
    }

    public function business_settings()
    {
        return $this->hasOne(BusinessSettings::class, 'business_id','business_id');
    }
}
