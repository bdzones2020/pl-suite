<?php

namespace App\Models\Business;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Business\Business;

class BusinessSettings extends Model
{
    use HasFactory;

    protected $fillable = ['business_id','base_currency','base_fabric_unit','base_measurement_unit','base_language','default_print_layout','fabric_dependency','stock_dependency','web_page','email_notification','sms_notification','has_employee_management','enable_stock_limit_warning','allow_only_fabric_sale','allow_external_fabric','default_order_slip_layout','disable_default_order_status','allow_online_order'];

    public function business()
    {
        return $this->belongsTo(Business::class);
    }
}
