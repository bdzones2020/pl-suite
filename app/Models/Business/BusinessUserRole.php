<?php

namespace App\Models\Business;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BusinessUserRole extends Model
{
    use HasFactory;

    protected $fillable = ['business_id','name','slug','weight','short_details','permissions','status'];
}
