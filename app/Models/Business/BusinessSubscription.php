<?php

namespace App\Models\Business;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Business\Business;

class BusinessSubscription extends Model
{
    use HasFactory;

    protected $fillable = ['business_id','package','price_policy','amount','transaction_method','transacton_code','paid_amount','transaction_mobile_number','status','valid_till'];

    public function business()
    {
        return $this->belongsTo(Business::class);
    }
}
