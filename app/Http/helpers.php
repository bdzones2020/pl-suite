<?php

use Illuminate\Support\Facades\URL;
use App\Models\Business\BusinessUser;
use Illuminate\Support\Facades\Auth;

function appInfo()
{
	return json_decode(file_get_contents(config_path('appInfo.json')), true);
}

function packageFeatures()
{
    return json_decode(file_get_contents(config_path('packageFeatures.json')), true);
}

function orderStatus()
{
	return json_decode(file_get_contents(config_path('orderStatus.json')), true);
}

function orderStatusColor($status)
{
    $statusList = json_decode(file_get_contents(config_path('orderStatus.json')), true);
	foreach($statusList as $s => $getStatus){
		if($s == $status){
			return $getStatus['color'];
		}
	}
}

function orderStatusWeight($status)
{
    $statusList = json_decode(file_get_contents(config_path('orderStatus.json')), true);
	foreach($statusList as $s => $getStatus){
		if($s == $status){
			return $getStatus['weight'];
		}
	}
}

function getBusiness($businessSlug)
{
	if(Auth::check()){
		return BusinessUser::with('business')->where('user_id',Auth::user()->id)->where('business_slug',$businessSlug)->first()->business;
	}
}

function getBusinessDetails($businessSlug)
{
	if(Auth::check()){
		return BusinessUser::with('business.business_settings')->where('user_id',Auth::user()->id)->where('business_slug',$businessSlug)->first()->business;
	}
}

function slugToTitle($slug)
{
	return ucwords(str_replace('-', ' ', $slug));
}

function decimalToSlash($decimal)
{
	if(strpos($decimal,'.'))
	{
		$integer = explode('.',$decimal)[0];
		$fraction = explode('.',$decimal)[1];
		if($fraction == '25'){
			$fraction = '<div class="fraction"><div class="top">'.(1).'</div><div class="bottom">'.(4).'</div></div>';
		}elseif($fraction == '50' || $fraction == '5'){
			$fraction = '<div class="fraction"><div class="top">'.(1).'</div><div class="bottom">'.(2).'</div></div>';
		}elseif($fraction == '75'){
			$fraction = '<div class="fraction"><div class="top">'.(3).'</div><div class="bottom">'.(4).'</div></div>';
		}
		$decimal = '<div class="fraction-box"><div class="number">'.strval($integer).'</div>'.$fraction.'</div>';
	}else{
		$decimal = '<div class="fraction-box"><div class="number">'.strval($decimal).'</div></div>';
	}

	return $decimal;
}

function packageDuration($pricePolicy)
{
	return $pricePolicy == 'monthly' ? date('Y-m-d', strtotime('+30 days')) : date('Y-m-d', strtotime('+365 days'));
}