<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WebController extends Controller
{
    public function home()
    {
        return view('web.home');
    }

    public function about_us()
    {
        return view('web.about_us');
    }

    public function services()
    {
        return view('web.services');
    }

    public function pricing()
    {
        return view('web.pricing');
    }

    public function faq()
    {
        return view('web.faq');
    }

    public function terms_conditions()
    {
        return view('web.terms_conditions');
    }

    public function privacy_policy()
    {
        return view('web.privacy_policy');
    }

    public function contact()
    {
        return view('web.contact');
    }

    public function send_message(Request $request)
    {
        $this->validate($request,[
            'name' => 'required|string|max:50',
            'email' => 'required|string|email|max:50',
            'contact_number' => 'required|string|max:11|min:11',
            'subject' => 'required|string|max:50',
            'message' => 'required|string',
        ]);

        $details = [
            'name' => $request->name,
            'email' => $request->email,
            'contact_number' => $request->contact_number,
            'subject' => $request->subject,
            'message' => $request->message
        ];

        Mail::to($details['email'])->send(new PublicMessageMail($details));

        return redirect()->route('contact')->with('message_success','Your message has been submitted successfully. Thanks for your appreciation.');
    }
}