<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Business\Business;
use App\Models\Business\BusinessUser;

use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    protected function checkBusiness()
    {
        $checkBusiness = BusinessUser::where('user_id',Auth::user()->id)->first();
        if(!$checkBusiness)
        {
            return redirect()->route('dashboard.business-info');
        }
        return true;
    }

    public function index($businessSlug)
    {
        if($this->checkBusiness())
        {
            $business = getBusiness($businessSlug);

            return view('dashboard.home', compact('businessSlug','business'));
        }
    }
}
