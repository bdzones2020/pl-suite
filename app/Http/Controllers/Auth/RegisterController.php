<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use App\Models\Business\Business;
use App\Models\Business\BusinessUser;
use App\Models\Business\BusinessSettings;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'contact_no' => ['required', 'string', 'max:11', 'min:11', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'type' => ['required'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        if(!$data['gender'])
        {
            $user = User::create([
                'name' => $data['name'],
                'email' => $data['email'],
                'contact_no' => $data['contact_no'],
                'password' => Hash::make($data['password']),
                'type' => $data['type'],
                'weight' => $data['type'] == 'owner' ? 49.99 : 9.99,
                'status' => 'active',
            ]);

            if($data['type'] == 'owner')
            {
                $business = new Business;
                $business->user_id = $user->id;
                $business->business_name = strtoupper($data['business_name']);
                $business->business_slug = Str::slug($data['business_name']);
                $business->business_contact_numbers = $data['contact_no'];
                $business->business_email = $data['email'];
                $business->valid_till = date('Y-m-d', strtotime(config('app.trial_duration')));
                $business->save();

                $business_user = new BusinessUser;
                $business_user->user_id = $user->id;
                $business_user->business_id = $business->id;
                $business_user->business_slug = $business->business_slug;
                $business_user->business_user_role_id = 1;
                $business_user->status = 'active';
                $business_user->save();

                $business_settings = new BusinessSettings;
                $business_settings->business_id = $business->id;
                $business_settings->save();
            }

            return $user;

        }else{
            return redirect()->route('/')->with('message_warning','Sorry, bots are not allowed!');
        }
    }
}
