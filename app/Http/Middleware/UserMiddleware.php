<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

use Auth;

class UserMiddleware
{
    public function handle(Request $request, Closure $next)
    {
        if (Auth::user() && Auth::user()->weight >= 9.99 && Auth::user()->status == true)
        {
            return $next($request);
        }
        return redirect('home')->with('message_warning','Sorry, you are not permitted to enter here. Please contact to your system admin.');
    }
}
