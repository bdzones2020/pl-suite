<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use App\Models\Business\BusinessUser;
use Illuminate\Support\Facades\DB;

class DashboardMiddleware
{
    public function handle(Request $request, Closure $next)
    {
        $user = Auth::user();
        if ($user->weight >= 9.99 && strtoupper($user->status) == true)
        {
            $check_user = BusinessUser::select('id','business_id','user_id','business_slug','business_user_role_id','status')
                ->with(['business_user_role','business_settings'])
                ->where('user_id',$user->id)
                ->where('business_slug',$request->route('businessSlug'))
                ->first();
            if (!empty($check_user))
            {
                if(strpos($check_user->business_user_role->permissions, 'dashboard,all') !== false)
                {
                    return $next($request);
                }elseif($request->route()->getName() == 'dashboard./')
                {
                    return $next($request);
                }elseif(strpos($check_user->business_user_role->permissions, str_replace('dashboard.', '', $request->route()->getName()) ) !== false ){
                    return $next($request);
                }else{
                    return redirect()->back()->with('message_warning','Sorry, permission denied!.');
                }
            }
        }
        return redirect('/')->with('message_warning','Sorry, you are not permitted to enter here. Please contact to your System Admin.');
    }
}
